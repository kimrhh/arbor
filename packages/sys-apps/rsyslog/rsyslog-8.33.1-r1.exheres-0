# Copyright 2010-2012 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ] systemd-service
require openrc-service [ openrc_confd_files=[ "${FILES}"/openrc/confd ] ]

SUMMARY="An enhanced syslog daemon for Linux (and Unix)"
DESCRIPTION="
rsyslog is an enhanced multi-threaded syslogd. Among others, it offers support
for on-demand disk buffering, reliable syslog over TCP, SSL, TLS, and RELP,
writing to databases (MySQL, PostgreSQL, Oracle, and many more), email alerting,
fully configurable output formats (including high-precision timestamps), the
ability to filter on any part of the syslog message, on-the-wire message
compression, and the ability to convert text files to syslog. It is a drop-in
replacement for stock syslogd and able to work with the same configuration file
syntax.
"
HOMEPAGE="https://www.${PN}.com"
DOWNLOADS="${HOMEPAGE}/files/download/${PN}/${PNV}.tar.gz"

BUGS_TO=""
REMOTE_IDS="freecode:${PN}"

UPSTREAM_CHANGELOG="${HOMEPAGE}/changelog-for-${PV/./-}-v$(ever major)-stable.txt [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/doc/manual.html [[ lang = en ]]"

LICENCES="GPL-3 LGPL-3"
SLOT="4"
PLATFORMS="~amd64 ~armv7 ~ppc64 ~x86"
MYOPTIONS="
    curl       [[ description = [ Support http_request() function in RainerScript ] ]]
    debug
    gcrypt     [[ description = [ Support log file encryption ] ]]
    gnutls
    kerberos
    mysql      [[ description = [ Support logging to a MySQL database ] ]]
    omhttpfs   [[ description = [ Build the httpfs output module ] ]]
    postgresql [[ description = [ Support logging to a PostgreSQL database ] ]]
    relp       [[ description = [ Support relp network protocol ] ]]
    systemd
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
"

# Tons and tons of sandbox violations, some tests need root permissions, some want
# to write to the live fs.
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
        sys-devel/bison
    build+run:
        dev-libs/libee[>=0.3.1]
        dev-libs/libfastjson[>=0.99.8]
        dev-libs/libestr[>=0.1.9]
        sys-apps/util-linux [[ note = [ for libuuid ] ]]
        curl? ( net-misc/curl )
        gcrypt? ( dev-libs/libgcrypt )
        gnutls? ( dev-libs/gnutls )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        mysql? ( virtual/mysql )
        omhttpfs? ( net-misc/curl )
        postgresql? ( dev-db/postgresql )
        relp? ( net-libs/librelp[>=1.2.5] )
        systemd? ( sys-apps/systemd[>=209] )
    suggestion:
        app-admin/logrotate [[ description = [ Support for rotating log files ] ]]
"

src_configure() {
    local myconf=(
        --enable-imdiag
        --enable-imptcp
        --enable-imfile
        --enable-inet
        --enable-klog
        --enable-mail
        --enable-omprog
        --enable-omruleset
        --enable-omuxsock
        --enable-regexp
        --enable-rsyslogd
        --enable-rsyslogrt
        --enable-uuid
        --disable-generate-man-pages
        --disable-imczmq
        --disable-imkafka
        --disable-imzmq3
        --disable-kafka-tests
        --disable-ksi-ls12
        --disable-libfaketime
        --disable-liblogging-stdlog
        --disable-mmdblookup
        --disable-mmnormalize
        --disable-mmrfc5424addhmac
        --disable-omamqp1
        --disable-omczmq
        --disable-omhdfs
        --disable-omhiredis
        --disable-omkafka
        --disable-omrabbitmq
        --disable-omzmq3
        --disable-rfc3195
        --disable-root-tests
        --disable-valgrind
        --without-valgrind-testbench

        # This is not a mistake and passing it to configure is correct.
        # Java is not being used at all yet, however the tests depend on it.
        HAVE_JAVAC=yes

        $(option_enable curl libcurl)
        $(option_enable debug)
        $(option_enable debug diagtools)
        $(option_enable gcrypt libgcrypt)
        $(option_enable gnutls)
        $(option_enable kerberos gssapi-krb5)
        $(option_enable mysql)
        $(option_enable omhttpfs)
        $(option_enable postgresql pgsql)
        $(option_enable relp)
        $(option_enable systemd imjournal)
        $(option_enable systemd libsystemd)
        $(option_enable systemd omjournal)

        $(option_with systemd systemdsystemunitdir ${SYSTEMDSYSTEMUNITDIR})
    )

    if expecting_tests; then
        myconf+=( --enable-{testbench{1,2},extended-tests} )
    else
        myconf+=( --disable-{testbench{1,2},extended-tests} )
    fi

    econf "${myconf[@]}"
}

src_install() {
    default

    # WorkDirectory (StateFile location)
    keepdir /var/lib/rsyslog

    # Default config
    insinto /etc
    doins "${FILES}"/rsyslog.conf
    insinto /etc/rsyslog.d
    doins "${FILES}"/50-default.conf

    if option systemd ; then
        insinto /etc/rsyslog.d
        doins "${FILES}"/50-systemd.conf
        insinto /etc/logrotate.d
        newins "${FILES}"/rsyslog.logrotate.systemd rsyslog
    else
        insinto /etc/logrotate.d
        newins "${FILES}"/rsyslog.logrotate rsyslog
    fi

    edo rmdir "${IMAGE}"/usr/share/man/man1

    install_openrc_files
}

