[binaries]
c = 'x86_64-pc-linux-musl-cc'
cpp = 'x86_64-pc-linux-musl-c++'
ar = 'x86_64-pc-linux-musl-ar'
strip = 'x86_64-pc-linux-musl-strip'
pkgconfig = 'x86_64-pc-linux-musl-pkg-config'

[host_machine]
system = 'linux'
cpu_family = 'x86_64'
cpu = 'x86_64'
endian = 'little'
