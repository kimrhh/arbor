[binaries]
c = 'i686-pc-linux-gnu-cc'
cpp = 'i686-pc-linux-gnu-c++'
ar = 'i686-pc-linux-gnu-ar'
strip = 'i686-pc-linux-gnu-strip'
pkgconfig = 'i686-pc-linux-gnu-pkg-config'

[host_machine]
system = 'linux'
cpu_family = 'x86'
cpu = 'i686'
endian = 'little'
