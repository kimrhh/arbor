[binaries]
c = 'aarch64-unknown-linux-gnueabi-cc'
cpp = 'aarch64-unknown-linux-gnueabi-c++'
ar = 'aarch64-unknown-linux-gnueabi-ar'
strip = 'aarch64-unknown-linux-gnueabi-strip'
pkgconfig = 'aarch64-unknown-linux-gnueabi-pkg-config'

[host_machine]
system = 'linux'
cpu_family = 'arm'
cpu = 'aarch64'
endian = 'little'
