# Copyright 2007 Bryan Østergaard
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Apache portable runtime library"
HOMEPAGE="https://${PN}.apache.org"
DOWNLOADS="mirror://apache/${PN}/${PNV}.tar.bz2"

REMOTE_IDS="freecode:${PN}"

LICENCES="Apache-2.0"
SLOT="1"
PLATFORMS="~amd64 ~arm ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        sys-apps/util-linux [[ note = [ for libuuid ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.3.9-sydbox.patch
    "${FILES}"/${PN}-1.5.0-libtool.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-posix-shm
    --enable-threads
    --disable-static
    --with-installbuilddir=/usr/$(exhost --target)/lib/${PN}-build-$(ever major)
)
DEFAULT_SRC_TEST_PARAMS=( -j1 )

AT_M4DIR=( build )

src_prepare() {
    # workaround issue with libtool 2.4.3
    elibtoolize --install --copy --force

    autotools_src_prepare
}

src_configure() {
    # FIXME: Two more configure tests try to bind 0.0.0.0:0
    # Since this is a net-sandboxing mess, we turn it off for now.
    esandbox disable_net
#   esandbox allow_net "unix:${WORK}/apr_accept4_test_socket"
#   esandbox allow_net --connect "unix:${WORK}/apr_accept4_test_socket"
    default
#   esandbox disallow_net "unix:${WORK}/apr_accept4_test_socket"
#   esandbox disallow_net --connect "unix:${WORK}/apr_accept4_test_socket"
    esandbox enable_net
}

src_test() {
    esandbox allow_net "inet:0.0.0.0@8021"
    esandbox allow_net "inet:192.0.2.1@8080"
    esandbox allow_net "unix:/tmp/apr-socket"

    default

    esandbox disallow_net "unix:/tmp/apr-socket"
    esandbox disallow_net "inet:192.0.2.1@8080"
    esandbox disallow_net "inet:0.0.0.0@8021"
}

src_install() {
    default

    # only used on AIX systems
    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/apr.exp
}

