# Copyright 2009 Bryan Østergaard
# Copyright 2012-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service
require python [ blacklist='2' multibuild=false with_opt=true ]

export_exlib_phases src_test src_install

SUMMARY="Support utilities for handling btrfs filesystems"
HOMEPAGE="https://btrfs.wiki.kernel.org"

BUGS_TO="philantrop@exherbo.org"

UPSTREAM_CHANGELOG="${HOMEPAGE}/index.php?title=Changelog"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/index.php?title=Main_Page#Documentation"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-doc/asciidoc
        app-text/xmlto
        virtual/pkg-config[>=0.9.0]
        python? ( dev-python/setuptools[python_abis:*(-)?] )
    build+run:
        app-arch/lzo:2
        app-arch/zstd[>=1.0.0]
        sys-apps/acl
        sys-apps/util-linux
        sys-fs/e2fsprogs[>=1.42]
    test:
        sys-fs/lvm2 [[ note = dmsetup ]]
    suggestion:
        dev-python/matplotlib [[ description = [ for btrfs-show-blocks & other debugging tools ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-zstd
    # There's also support to convert reiserfs, but it needs
    # reiserfsprogs[>=3.6.27] which we don't have yet.
    --with-convert=ext2
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( python )

DEFAULT_SRC_COMPILE_PARAMS=(
    V=1
)

DEFAULT_SRC_INSTALL_PARAMS=(
    mandir=/usr/share/man
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=(
    INSTALL
    show-blocks
)

AT_M4DIR=( m4 )

btrfs-progs_src_test() {
    # prevent trying to run modprobe
    edo sed \
        -e 's:run_check $SUDO_HELPER modprobe btrfs::g' \
        -i tests/common

    edo mkdir "${TEMP}"/{dev,mnt}
    edo sed \
        -e "/TEST_DEV=/s:$:${TEMP}/dev:" \
        -e "/TEST_MNT=/s:$:${TEMP}/mnt:" \
        -i tests/fsck-tests.sh

    emake -j1 test
}

btrfs-progs_src_install() {
    if option python ; then
        emake -j1 DESTDIR="${IMAGE}" "${DEFAULT_SRC_INSTALL_PARAMS[@]}" install install_python
        emagicdocs
    else
        default
    fi

    install_systemd_files
    # For potentially critical filesystem services (e. g. btrfs, lvm2) we auto-
    # activate the respective service.
    dodir "${SYSTEMDSYSTEMUNITDIR}"/basic.target.wants
    dosym ../btrfs.service "${SYSTEMDSYSTEMUNITDIR}"/basic.target.wants/btrfs.service
}

