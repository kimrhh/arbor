# Copyright 2009 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'e2fsprogs-1.40.9.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require sourceforge [ suffix=tar.gz ]

export_exlib_phases src_configure src_test src_install

SUMMARY="Utilities for managing ext2/ext3/ext4 filesystems"
DOWNLOADS+=" mirror://kernel/linux/kernel/people/tytso/${PN}/v${PV}/${PNV}.tar.gz"

LICENCES="
    GPL-2
    LGPL-2  [[ note = [ ext2fs and e2p libraries ] ]]
    MIT [[ note = [ et and ss libraries ] ]]
"
SLOT="0"
MYOPTIONS="
    fuse [[ description = [ FUSE file system client for ext2/ext3/ext4 file systems ] ]]
    ( linguas: ca cs da de eo es fi fr hu id it ms nl pl sr sv tr uk vi zh_CN )
"

# TODO: Find out why most tests are failing under sydbox since 1.42.10
# 58 tests succeeded / 265 tests failed (last checked: 1.43)
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-apps/texinfo
        sys-devel/gettext[>=0.14.1]
        virtual/pkg-config[>=0.20]
    build+run:
        sys-apps/util-linux[>=2.16.2] [[ note = [ to ensure that libblkid and libuuid from util-linux is used ] ]]
        fuse? ( sys-fs/fuse:0[>=2.9] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-bmap-stats
    --enable-elf-shlibs
    --enable-mmp
    --enable-nls
    --enable-symlink-install
    --enable-tls
    --enable-verbose-makecmds
    --disable-bmap-stats-ops
    --disable-fsck
    --disable-libblkid
    --disable-libuuid
    --disable-tdb
    --disable-uuidd
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'fuse fuse2fs'
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( RELEASE-NOTES )
DEFAULT_SRC_INSTALL_PARAMS=( install-libs )

e2fsprogs_src_configure() {
    # Keep the package from doing silly things
    export ac_cv_path_LDCONFIG=:
    export STRIP=:

    local myconf=(
        "${DEFAULT_SRC_CONFIGURE_PARAMS[@]}" \
        $(for s in "${DEFAULT_SRC_CONFIGURE_OPTION_ENABLES[@]}" ; do \
            option_enable ${s} ; \
        done )
    )

    BUILD_CC=$(exhost --build)-cc \
        econf ${myconf[@]}
}

e2fsprogs_src_test() {
    # Not everyone has 2TB of free space.
    edo rm -rf tests/{r_64bit_big_expand,r_bigalloc_big_expand,r_ext4_big_expand}

    # The test script uses $IMAGE for its own purposes.
    local IMAGE
    # the default blkid file is /etc/blkid.tab which results in sydbox violations
    BLKID_FILE="${HOME}/blkid.tab" default
}

e2fsprogs_src_install() {
    default

    # e2fsprogs has references to the build directory here
    edo sed -i -e '/^ET_DIR=/s:=.*:=/usr/share/et:' \
        "${IMAGE}"/usr/$(exhost --target)/bin/compile_et
    edo sed -i -e '/^SS_DIR=/s:=.*:=/usr/share/ss:' \
        "${IMAGE}"/usr/$(exhost --target)/bin/mk_cmds
}

