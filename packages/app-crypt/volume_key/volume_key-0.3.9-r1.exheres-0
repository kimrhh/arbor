# Copyright 2017 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pagure [ suffix=tar.xz ] \
    python [ blacklist=none multibuild=false has_bin=false has_lib=true ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Tool and library for manipulating and storing storage volume encryption keys"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

# fails tests
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.17]
        virtual/pkg-config[>=0.9.0]
    build+run:
        app-crypt/gnupg
        app-crypt/gpgme[>=0.4.2]
        dev-libs/glib:2
        dev-libs/nss
        sys-apps/util-linux [[ note = [ libblkid ] ]]
        sys-fs/cryptsetup
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/8f8698aba19b501f01285e9eec5c18231fc6bcea.patch
    "${FILES}"/${PNV}-Stop-using-crypt_get_error.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
)

src_prepare() {
    if [[ $(python_get_abi) == 3.* ]] ; then
        edo sed \
            -e "s/-lpython\$(PYTHON_VERSION)/-lpython${PYTHON_ABIS}m/" \
            -i Makefile.am
    fi

    autotools_src_prepare
}

src_compile() {
    emake PYTHON_CPPFLAGS=$(${PKG_CONFIG} --cflags python-${PYTHON_ABIS})
}

